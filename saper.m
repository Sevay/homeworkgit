function [varargout]=saper(varargin)

%%czyszcznie
clc
clear
close all

h=struct; %%utworzenie glownego uchwytu gry

h.row=10; %%liczba wierszy
h.column=10; %%liczba kolumn
h.iposx=350;h.iposy=100; %%pozycja na ekranie
h.boxdim=[35 35]; %%rozmiar przyciskow
h.gap=[0.5 0.5]; %%przestrzen miedzy przyciskami
h.mine=imread(strcat('bomba','.png')); %%symbol miny, pobrany z pliku

% tworzenie GUI

%glowne okno
h.main=figure('name','Saper','NumberTitle',...
    'off','Position',[h.iposx,h.iposy,(h.row+4)*35,(h.column+4)*35], 'MenuBar', 'none', 'Toolbar', 'none',...
    'Resize', 'off');
%przycisk resetu
h.restart=uicontrol(h.main,'style','pushbutton','position',...
    [190,435,100,35],'string','Restart gry','callback','saper','tooltipstring','Restart gry');

%%tworzenie przyciskow gry
for x=1:h.row+2
    for y=1:h.column+2
        h.box(y,x)=uicontrol(h.main,'style','pushbutton','FontWeight','bold','foregroundcolor','b','fontsize',12,...
            'position',[h.boxdim(1)*(x),h.boxdim(2)*(h.column+3-y),h.boxdim(1)-h.gap(1),h.boxdim(2)-h.gap(2)]);
        if x==1 || y==1 || x==h.row+2 || y==h.column+2
            set(h.box(y,x),'visible','off');
        end
    end
end

% tworzenie srodowiska obslugi bomb
h.difficulty=0.65;
randMatrix=rand(h.row+2,h.column+2); %%tworzenie matrixa z wylosowanymi polami min
bomb=int8(h.difficulty*randMatrix); %%powy�ej 0.5 przejdzie na 1

bomb(:,1)=zeros(h.row+2,1); %zerowanie pierwszej kolumny
bomb(:,h.column+2)=zeros(h.row+2,1); %%zerowanie ostatniej kolumny
bomb(1,:)=zeros(1,h.column+2); %%zerowanie pierwszego wiersza
bomb(h.row+2,:)=zeros(1,h.column+2); %%zerowanie ostatniego wiersza

h.game=bomb*9; %%zaznaczenie w tablicy ka�dej bomby jako 9

for x=2:h.row+1 
    for y=2:h.column+1
        n=0;
        if bomb(x,y)==0
            if bomb(x-1,y-1)==1
                n=n+1;
            end
            if bomb(x-1,y)==1
                n=n+1;
            end
            if bomb(x-1,y+1)==1
                n=n+1;
            end
            if bomb(x,y-1)==1
                n=n+1;
            end
            if bomb(x,y+1)==1
                n=n+1;
            end
            if bomb(x+1,y-1)==1
                n=n+1;
            end
            if bomb(x+1,y)==1
                n=n+1;
            end
            if bomb(x+1,y+1)==1
                n=n+1;
            end
        h.game(x,y)=n;    
        end 
    end
end

%%ustawienie obs�ugi przycisk�w gry
for ii=1:h.row+2
    for jj=1:h.column+2
        set(h.box(ii,jj),'callback',{'button',h},'buttondownfcn',{'mark',h});
    end
end
end